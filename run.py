""" Basic restful using webpy 0.3 """
#import web to http
import web
#import model to db function
import model
#import json to request format
import json
#import request access api rest twilio
import requests
from requests.auth import HTTPDigestAuth

#import pycurl



### Urls
urls = (
    '/api/sms', 'Index',
    '/api/sms/(.+)', 'Show'
)


### Templates
render = web.template.render('templates', base='base')

#Class Index with get and post
class Index:

    def GET(self):

        twilio_url = 'https://api.twilio.com/2010-04-01/Accounts/ACf8a5060ccb6c8fbc954f16c6068f0485/Messages.json'
        data = json.dumps({"From":"+19284408443","To":"+56950012660","Body":"test api rest 10" }) 
        resp = requests.post(twilio_url, data, auth=('ACf8a5060ccb6c8fbc954f16c6068f0485', 'b6963ab82fa977905c276f07e6668c41'))
        return resp

        #postData =  '{"From" : "+19284408443", "To" : "+56950012660", "Body":"test api rest 10"}'
        #c = pycurl.Curl()
        #c.setopt(pycurl.URL, 'https://api.twilio.com/2010-04-01/Accounts/ACf8a5060ccb6c8fbc954f16c6068f0485/SMS/Messages.json')
        #c.setopt(pycurl.HTTPHEADER, ['Accept: application/json'])
        #c.setopt(pycurl.HTTPHEADER, ['Content-Type : application/x-www-form-urlencoded'])
        #c.setopt(pycurl.POST, 1)
        #c.setopt(pycurl.POSTFIELDS, postData)
        #c.setopt(pycurl.USERPWD, 'ACf8a5060ccb6c8fbc954f16c6068f0485:b6963ab82fa977905c276f07e6668c41')
        #req = c.perform()
        #return req
        

        #j = json.loads(resp)
        #model.new_mesagge(j)

        #return json.dumps({"Body":"test api rest 10" })


#Class Show with Get
class Show:

    def GET(self, sid):
        twilio_url = 'https://api.twilio.com/2010-04-01/Accounts/ACf8a5060ccb6c8fbc954f16c6068f0485/Messages/'+sid+'.json'
        req = requests.get(twilio_url, auth=HTTPDigestAuth('ACf8a5060ccb6c8fbc954f16c6068f0485', 'b6963ab82fa977905c276f07e6668c41'))
        
        return req
        #data = model.get_message(sid)
        #return json.dumps({'body': data[0].body})

app = web.application(urls, globals())

if __name__ == '__main__':
    app.run()